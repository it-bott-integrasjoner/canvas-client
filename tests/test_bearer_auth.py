import uuid

import pytest

from canvas_client.auth import HttpBearerAuth


@pytest.fixture
def token():
    return str(uuid.uuid4())


@pytest.fixture
def bearer_auth(token):
    return HttpBearerAuth(token)


def test_init(token, bearer_auth):
    assert token == bearer_auth.token


def test_eq(token):
    auth_foo = HttpBearerAuth(token)
    auth_bar = HttpBearerAuth(token)
    assert auth_foo == auth_bar


def test_ne():
    auth_foo = HttpBearerAuth("foo")
    auth_bar = HttpBearerAuth("bar")
    assert auth_foo != auth_bar


def test_eq_not_implemented(bearer_auth):
    class UniqueException(Exception):
        pass

    class Foo:
        def __eq__(self, other):
            if other is bearer_auth:
                raise UniqueException()

    with pytest.raises(UniqueException):
        # bearer_auth on lhs should trigger bearer_auth.__eq__, which should
        # delegate to Foo(), since Foo is incompatible.
        bearer_auth == Foo()


def test_header_value(token, bearer_auth):
    assert bearer_auth.value == "Bearer " + token


def test_apply(bearer_auth, a_request):
    bearer_auth(a_request)
    assert bearer_auth.header in a_request.headers
    assert a_request.headers[bearer_auth.header] == bearer_auth.value
