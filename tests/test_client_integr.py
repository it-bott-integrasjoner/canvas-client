import pytest

from canvas_client.client import get_client


@pytest.mark.integration
def test_list_courses_by_user(config):
    """List courses for user who is a teacher for given courses"""
    client = get_client(config)
    for course in client.list_courses_by_user("self"):
        assert course["id"] is not None
        assert course["name"] is not None


@pytest.mark.integration
def test_list_enrollments_by_course(config):
    client = get_client(config)

    course_list = client.list_courses_by_user("self")
    for course in course_list:
        course_id = course["id"]
        client = get_client(config)

        course = client.get_course(course_id=course_id)
        modules = client.list_course_modules(course_id=course_id)

        enrollments = client.list_enrollments_by_course(
            course_id=course_id, per_page=100, enrollment_type="StudentEnrollment"
        )

        assert course is not None
        assert enrollments is not None
        assert modules is not None


USER_KEYS = [
    "id",
    "name",
    "sortable_name",
    "short_name",
    "sis_user_id",
    "sis_import_id",
    "integration_id",
    "login_id",
]


@pytest.mark.integration
def test_list_users(config):
    client = get_client(config)
    for user in client.list_users():
        for k in USER_KEYS:
            assert k in user


@pytest.mark.integration
def test_user_methods(config):
    """
    Check various methods on user endpoints.

    Checks that we can create, get, update and delete a user, and
    checks that the created user gets a profile when created.
    """
    client = get_client(config)
    create_payload = {
        "user": {"name": "Integration Test Peter Parker"},
        "pseudonym": {"unique_id": "peterparker@example.com"},
    }
    update_payload = {
        "user": {"name": "Integration Test New Name"},
    }

    # Create, get, update, delete
    create = client.create_user(data=create_payload)
    get = client.get_user(user_id=create["id"])
    profile = client.get_user_profile(create["id"]).json()
    update = client.update_user(user_id=create["id"], data=update_payload)
    delete = client.delete_user(user_id=create["id"])
    # assert things about the responses
    assert create["name"] == "Integration Test Peter Parker"
    assert get["id"] == create["id"]
    assert get["name"] == "Integration Test Peter Parker"
    assert profile["id"] == create["id"]
    assert profile["name"] == "Integration Test Peter Parker"
    assert update["name"] == "Integration Test New Name"
    assert delete.get("user")
    assert delete["user"]["name"] == "Integration Test New Name"


# Calendar events tests
CALENDAR_EVENT_KEYS = [
    "id",
    "title",
    "start_at",
    "end_at",
    "description",
    "location_name",
    "location_address",
    "context_code",
    "effective_context_code",
    "context_name",
    "all_context_codes",
    "workflow_state",
    "hidden",
    "parent_event_id",
    "child_events_count",
    "child_events",
    "url",
    "html_url",
    "all_day_date",
    "all_day",
    "created_at",
    "updated_at",
    "appointment_group_id",
    "appointment_group_url",
    "own_reservation",
    "reserve_url",
    "reserved",
    "participant_type",
    "participants_per_appointment",
    "available_slots",
    "user",
    "group",
    "important_dates",
]


@pytest.mark.integration
def test_list_calendar_events(config):
    """Verify that we get elements with expected keys back"""
    client = get_client(config)
    for event in client.list_calendar_events():
        for k in CALENDAR_EVENT_KEYS:
            assert k in event


@pytest.mark.integration
def test_list_user_calendar_events(config):
    """Verify that we get elements with expected keys back"""
    client = get_client(config)
    user_id = 1
    for event in client.list_user_calendar_events(user_id):
        for k in CALENDAR_EVENT_KEYS:
            assert k in event


@pytest.mark.integration
def test_calendar_event_metods(config):
    """
    Verify that events are created with provided data.
    """
    client = get_client(config)
    create_payload = {}
    updatedata = {}
    # Create, get and delete an avent
    create = client.create_calendar_event(data=create_payload)
    get = client.get_calender_event(event_id=create["id"])
    update = client.update_calendar_event(event_id=create["id"], data=updatedata)
    delete = client.delete_calendar_event(event_id=create["id"])
    # assert things about the responses
    assert create
    assert get
    assert update
    assert delete
