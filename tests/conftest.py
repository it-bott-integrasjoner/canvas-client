import pytest
import requests
import yaml

from canvas_client.client import CanvasClient


@pytest.fixture
def url():
    return "https://localhost"


@pytest.fixture
def config():
    with open("config.yaml", "r") as f:
        content = f.read()
        return yaml.load(content, Loader=yaml.FullLoader)


@pytest.fixture
def client(url):
    return CanvasClient(url)


@pytest.fixture
def a_request():
    return requests.Request()


def pytest_collection_modifyitems(config, items):
    if config.option.keyword or config.option.markexpr:
        return
    skip_integration = pytest.mark.skip(
        reason='Not running with pytest -m "integration"'
    )
    for item in items:
        if "integration" in item.keywords:
            item.add_marker(skip_integration)
