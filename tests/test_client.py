import pytest
import requests

from canvas_client.client import CanvasClient, IncorrectPathError


@pytest.mark.parametrize(
    argnames=["use_sessions"],
    argvalues=[
        [True],
        [False],
        [requests.Session()],
    ],
)
def test___init___use_sessions(url, use_sessions):
    client = CanvasClient(
        url=url,
        use_sessions=use_sessions,
    )
    if use_sessions is True:
        assert isinstance(client.session, requests.Session)
    elif isinstance(use_sessions, requests.Session):
        assert client.session is use_sessions
    elif not use_sessions:
        assert client.session == requests
    else:
        assert False


def test_get_user(client: CanvasClient, requests_mock):
    user_id = 1
    expected = {}
    requests_mock.get(f"https://localhost/users/{user_id}", json=expected)
    result = client.get_user(user_id)
    assert result == expected


def test_get_user_no_data(client: CanvasClient, requests_mock):
    user_id = 1
    errors_string = '{"errors":[{"message":"The specified resource does not exist."}]}'
    json_data = {'_content': errors_string}
    requests_mock.get(f"https://localhost/users/{user_id}",
                      status_code=404,
                      json=json_data)
    result = client.get_user(user_id)
    assert result == None


def test_get_user_no_data(client: CanvasClient, requests_mock):
    user_id = 1
    json_data = {}
    requests_mock.get(f"https://localhost/users/{user_id}",
                      status_code=404,
                      json=json_data)
    with pytest.raises(IncorrectPathError):
        result = client.get_user(user_id)


def test_get_user_profile(client: CanvasClient, requests_mock):
    user_id = 1
    expected = {}
    requests_mock.get(f"https://localhost/users/{user_id}/profile", json=expected)
    result = client.get_user_profile(user_id, json=True)
    assert result == expected


def test_list_users(client: CanvasClient, requests_mock):
    expected = []
    requests_mock.get(
        f"https://localhost/accounts/self/users?per_page=100",
        json=expected,
        headers={
            "Link": '<https://localhost/users/{user_id}/calendar_events?per_page=100>; rel="current",'
        },
    )
    result = list(client.list_users())
    assert result == expected


def test_create_user(client: CanvasClient, requests_mock):
    data = {
        "user": {"name": "Peter Parker"},
        "pseudonym": {"unique_id": "peterparker@example.com"},
    }
    requests_mock.post(f"https://localhost/accounts/self/users", json=data)
    result = client.create_user(data)
    assert result == data


# Calendar events tests
def test_list_calendar_events(client: CanvasClient, requests_mock):
    expected = []
    requests_mock.get(
        "https://localhost/calendar_events?per_page=100",
        json=expected,
        headers={
            "Link": '<https://localhost/calendar_events?per_page=100>; rel="current",'
        },
    )
    result = list(client.list_calendar_events())
    assert result == expected


def test_list_user_calendar_events(client: CanvasClient, requests_mock):
    user_id = 1
    expected = []
    requests_mock.get(
        f"https://localhost/users/{user_id}/calendar_events?per_page=100",
        json=expected,
        headers={
            "Link": '<https://localhost/users/{user_id}/calendar_events?per_page=100>; rel="current",'
        },
    )
    result = list(client.list_user_calendar_events(user_id))
    assert result == expected


def test_create_calendar_event(client: CanvasClient, requests_mock):
    expected = {}
    requests_mock.post("https://localhost/calendar_events", json=expected)
    result = client.create_calendar_event(data=expected)
    assert result == expected


def test_get_calender_event(client: CanvasClient, requests_mock):
    event_id = 1
    expected = {}
    requests_mock.get(f"https://localhost/calendar_events/{event_id}", json=expected)
    result = client.get_calender_event(event_id)
    assert result == expected


def test_reserve_time_slot(client: CanvasClient, requests_mock):
    event_id = 1
    expected = {}
    requests_mock.post(
        f"https://localhost/calendar_events/{event_id}/reservations", json=expected
    )
    result = client.reserve_time_slot(event_id)
    assert result == expected


def test_update_calendar_event(client: CanvasClient, requests_mock):
    event_id = 1
    expected = {}
    requests_mock.put(f"https://localhost/calendar_events/{event_id}", json=expected)
    result = client.update_calendar_event(event_id, expected)
    assert result == expected


def test_delete_calendar_event(client: CanvasClient, requests_mock):
    event_id = 1
    expected = {}
    requests_mock.delete(f"https://localhost/calendar_events/{event_id}", json=expected)
    result = client.delete_calendar_event(event_id)
    assert result == expected


def test_create_course_timetable(client: CanvasClient, requests_mock):
    course_id = 1
    expected = {}
    requests_mock.post(
        f"https://localhost/courses/{course_id}/calendar_events/timetable",
        json=expected,
    )
    result = client.create_course_timetable(course_id, expected)
    assert result == expected
