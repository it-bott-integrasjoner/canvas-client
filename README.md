# canvas_client

`canvas_client` is a Python library for accessing Instructure's [Canvas
LMS][canvas-lms] [API][canvas-api].

[canvas-lms]: https://www.canvaslms.com/
[canvas-api]: https://canvas.instructure.com/doc/api/index.html

## Install

```sh
python setup.py install
```

## Tests

### Unit tests

```sh
python -m pytest
```

### Coverage

```sh
coverage run --branch --source=canvas_client -m pytest && coverage report
```

### Integration tests

Set credentials in `config.yaml` and run

```sh
python -m pytest -m integration
```

## Ignored commits

The file .git-blame-ignore-revs contains a list of commits that should be ignored when running `git blame`.
Run

```sh
$ git config blame.ignoreRevsFile .git-blame-ignore-revs
```

to configure git to ignore them for you. Please add a comment to any commit you put there.
