import requests


def parse_links(response):
    links = requests.utils.parse_header_links(
        response.headers["Link"].rstrip(">").replace(">,<", ",<")
    )
    return {link["rel"]: link["url"] for link in links}
