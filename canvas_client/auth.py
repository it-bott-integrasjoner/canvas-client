import requests.auth


class HttpBearerAuth(requests.auth.AuthBase):

    header = "Authorization"
    scheme = "Bearer"

    def __init__(self, token):
        self.token = str(token)

    def __repr__(self):
        return (
            "<{cls.__module__}.{cls.__name__} " "{scheme} ...' at 0x{addr:02x}>"
        ).format(cls=type(self), scheme=str(self.scheme), addr=id(self))

    @property
    def value(self):
        return self.scheme + " " + self.token

    def __eq__(self, other):
        if not hasattr(other, "token"):
            return NotImplemented
        return self.token == other.token

    def __ne__(self, other):
        return not self == other

    def __call__(self, r):
        r.headers[self.header] = self.value
        return r
