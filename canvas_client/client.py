import logging
from typing import Optional, Union, Dict, Any
import urllib.parse
import warnings
import json

import requests

from .auth import HttpBearerAuth
from .utils import parse_links

logger = logging.getLogger(__name__)

ROOT_ACCOUNT = "self"


def quote_path_arg(arg: str) -> str:
    return urllib.parse.quote(str(arg), safe="")


def merge_dicts(*dicts) -> Dict[Any, Any]:
    """
    Combine a series of dicts without mutating any of them.

    >>> merge_dicts({'a': 1}, {'b': 2})
    {'a': 1, 'b': 2}
    >>> merge_dicts({'a': 1}, {'a': 2})
    {'a': 2}
    >>> merge_dicts(None, None, None)
    {}
    """
    combined = dict()
    for d in dicts:
        if not d:
            continue
        for k in d:
            combined[k] = d[k]
    return combined


class IncorrectPathError(Exception):
    pass


class CanvasClient:
    default_headers = {}
    default_account = ROOT_ACCOUNT

    def __init__(
        self,
        url,
        token=None,
        default_account=default_account,
        headers=None,
        rewrite_url=None,
        use_sessions=True,
    ):
        """
        Canvas LMS API client.

        :param str url: Url to the base of the Canvas API
        :param str token: Canvas API Bearer authorization token
        :param str default_account:
            The ID of a default subaccount. This subaccount is used when
            creating new sub-accounts with no parent.
        :param dict headers: Default headers for all requests
        :param bool|requests.Session use_sessions:
            Re-use connections (default: true)
            Use an instance of requests.Session if you need more control
            over the http session
        """
        self.url = url
        self.auth = token
        self.headers = merge_dicts(self.default_headers, headers)
        self.default_account = default_account
        self.rewrite_url = rewrite_url
        if use_sessions:
            if isinstance(use_sessions, requests.Session):
                self.session = use_sessions
            else:
                self.session = requests.Session()
        else:
            self.session = requests

    def __repr__(self) -> str:
        return "<{cls.__name__} url={obj.url!r}>".format(cls=type(self), obj=self)

    @property
    def auth(self):
        return self.__auth

    @auth.setter
    def auth(self, token: str):
        if token is not None:
            self.__auth = HttpBearerAuth(token)
        else:
            self.__auth = None

    def call(self,
             method_name: str,
             path: str,
             absolute: Optional[bool] = None,
             headers=None,
             **kwargs):
        """"""
        url = urllib.parse.urljoin("" if absolute else self.url, path)
        if self.rewrite_url is not None:
            url = url.replace(*self.rewrite_url)
        headers = merge_dicts(self.headers, headers)
        logger.debug("Calling %s url:%s with %r", method_name, url, kwargs)
        r = self.session.request(
            method_name, url, auth=self.auth, headers=headers, **kwargs
        )
        if r.status_code in (500, 400, 401):
            logger.warn("Got HTTP %d: %r", r.status_code, r.content)
        if r.status_code == 404:
            try:
                response_content = r._content
                if "errors" in json.loads(response_content.decode('utf-8'))["_content"]:
                    return None
                else:
                    raise IncorrectPathError
            except:
                raise IncorrectPathError
        r.raise_for_status()
        return r

    def get(self, path: str, **kwargs):
        return self.call("GET", path, **kwargs)

    def post(self, path: str, **kwargs):
        return self.call("POST", path, **kwargs)

    def put(self, path: str, **kwargs):
        return self.call("PUT", path, **kwargs)

    def delete(self, path: str, **kwargs):
        return self.call("DELETE", path, **kwargs)

    def depaginate(self, page: str, in_envelope: Optional[str]=None):
        """
        Yield all items, following links from `page`.
        """
        while page:
            items = page.json()
            if in_envelope:
                items = items.get(in_envelope)
            for item in items:
                yield item
            next_page = parse_links(page).get("next")
            if next_page:
                page = self.get(next_page, absolute=True)
            else:
                break

    def get_user(self, user_id: int) -> Optional[str]:
        """
        Show user details.

        https://canvas.instructure.com/doc/api/users.html#method.users.api_show
        """
        user_id = quote_path_arg(user_id)
        user = self.get(f"users/{user_id}")
        if user:
            return user.json()
        return None

    def get_user_profile(self,
                         user_id: int,
                         json: Optional[bool]=False
        ) -> Union[str, requests.Response]:
        """
        Get user profile.

        https://canvas.instructure.com/doc/api/users.html#method.profile.settings
        """
        user_id = quote_path_arg(user_id)
        response = self.get(f"users/{user_id}/profile")
        if json:
            return response.json()
        return response

    def list_users(self,
        per_page: int =100,
        search_term: Optional[str] = None,
        sort: Optional[Any] = None,
        order: Optional[Any] = None):
        """
        List users associated with account.

        https://canvas.instructure.com/doc/api/users.html#method.users.index

        :param str search_term:
            Partial name or full SIS/login ID to search for. The API will
            prefer matching on canonical user ID if the ID has a numeric form.
        :param str sort: Sort results by: username/email/sis_id/last_login
        :param str order: Sort given parameter: asc/desc
        """
        account_id = quote_path_arg(ROOT_ACCOUNT)
        params = {
            "per_page": per_page,
        }
        if search_term is not None:
            params["search_term"] = search_term
        if sort is not None:
            params["sort"] = sort
        if order is not None:
            params["order"] = order
        first_page = self.get(f"accounts/{account_id}/users", params=params)
        for user in self.depaginate(first_page):
            yield user

    def create_user(self, data: dict, enable_sis_reactivation: bool = False) -> str:
        """
        Create a user.

        https://canvas.instructure.com/doc/api/users.html#method.users.create
        """
        account_id = quote_path_arg(ROOT_ACCOUNT)
        if "enable_sis_reactivation" not in data:
            data["enable_sis_reactivation"] = enable_sis_reactivation
        return self.post(f"accounts/{account_id}/users", json=data).json()

    def update_user(self, user_id: int, data: dict) -> str:
        """
        Update a user.

        https://canvas.instructure.com/doc/api/users.html#method.users.update
        """
        user_id = quote_path_arg(user_id)
        return self.put(f"users/{user_id}", json=data).json()

    def delete_user(self, user_id: int) -> str:
        """
        Delete a user from a Canvas root account.

        https://canvas.instructure.com/doc/api/accounts.html#method.accounts.remove_user
        """
        account_id = quote_path_arg(ROOT_ACCOUNT)
        user_id = quote_path_arg(user_id)
        return self.delete(
            f"accounts/{account_id}/users/{user_id}".format(
                account_id=account_id, user_id=user_id
            )
        ).json()

    def list_user_communication_channels(self, user_id: int) -> str:
        """
        List user communication channels

        https://canvas.instructure.com/doc/api/communication_channels.html#method.communication_channels.index
        """
        user_id = quote_path_arg(user_id)
        return self.get(f"users/{user_id}/communication_channels").json()

    def create_user_communication_channel(self, user_id: int, data: dict, skip_confirmation: bool = None) -> str:
        """
        Creates a communication channel for the given user

        https://canvas.instructure.com/doc/api/communication_channels.html#method.communication_channels.create
        """
        user_id = quote_path_arg(user_id)
        data = {"communication_channel": data}
        if skip_confirmation:
            data["skip_confirmation"] = skip_confirmation
        return self.post(f"users/{user_id}/communication_channels", json=data).json()

    def delete_user_communication_channel_id(self, user_id: int, comchan_id: int) -> str:
        """
        Delete a communication channel of a user

        https://canvas.instructure.com/doc/api/communication_channels.html#method.communication_channels.destroy
        """
        user_id = quote_path_arg(user_id)
        comchan_id = quote_path_arg(comchan_id)
        return self.delete(
            f"users/{user_id}/communication_channels/{comchan_id}"
        ).json()

    def delete_user_communication_channel_type_adr(self, user_id: int, adr_type, address) -> str:
        """
        Delete a communication channel of a user

        https://canvas.instructure.com/doc/api/communication_channels.html#method.communication_channels.destroy
        """
        user_id = quote_path_arg(user_id)
        adr_type = quote_path_arg(adr_type)
        address = quote_path_arg(address)
        return self.delete(
            f"users/{user_id}/communication_channels/{adr_type}/{address}"
        ).json()

    def create_user_login(self, user_id: int, login_data=None) -> requests.Response:
        """
        Create a user.

        https://canvas.instructure.com/doc/api/users.html#method.users.create
        """
        account_id = quote_path_arg(ROOT_ACCOUNT)
        login_data = login_data or {}
        return self.post(
            f"accounts/{account_id}/logins",
            json={"user": {"id": user_id}, "login": login_data},
        )

    def update_user_login(self, login_id: int, data: dict) -> requests.Response:
        """
        Update user-login

        https://canvas.instructure.com/doc/api/logins.html#method.pseudonyms.update
        """
        account_id = quote_path_arg(ROOT_ACCOUNT)
        login_id = quote_path_arg(login_id)
        return self.put(
            f"accounts/{account_id}/logins/{login_id}", json={"login": data}
        )

    def get_or_create_user_login(self,
                                 user_id: int,
                                 login_data: Optional[Dict] = None
                                ) -> str:
        """"""
        login_data = login_data or {}
        existing = False
        logins = self.list_user_logins(user_id)
        if logins.ok:
            # list existing logins
            for ex_login in logins.json():
                if ex_login.get("sis_user_id") == login_data["sis_user_id"]:
                    existing = True
                    user_login_data = ex_login
                    break
        if not existing:
            user_login_data = self.create_user_login(user_id, login_data=login_data)
        return user_login_data.json()

    def list_user_logins(self, user_id: int) -> str:
        """
        List logins associated with account and user_id.

        https://canvas.instructure.com/doc/api/logins.html#method.pseudonyms.index
        """
        user_id = quote_path_arg(user_id)
        return self.get(f"users/{user_id}/logins").json()

    def delete_user_login(self, user_id: int, login_id: int) -> str:
        """
        Delete a user-login

        https://canvas.instructure.com/doc/api/logins.html#method.pseudonyms.destroy
        """
        user_id = quote_path_arg(user_id)
        login_id = quote_path_arg(login_id)
        return self.delete(f"users/{user_id}/logins/{login_id}").json()

    def purge_user(self, user_id: int) -> requests.Response:
        """
        A wrapper-method that delete the user as well as all its logins

        Currently only used for testing purposes
        """
        logins = self.list_user_logins(user_id)
        if logins.ok:
            for login in logins.json():
                self.delete_user_login(user_id, login["id"])
        return self.delete_user(user_id)

    def list_accounts(self):
        first_page = self.get("accounts")
        for account in self.depaginate(first_page):
            yield account

    def list_terms(self, per_page: int =100):
        account_id = quote_path_arg(ROOT_ACCOUNT)
        params = {
            "per_page": per_page,
        }
        first_page = self.get(f"accounts/{account_id}/terms", params=params)
        for term in self.depaginate(first_page, in_envelope="enrollment_terms"):
            yield term

    def get_account(self, account_id: Optional[str] = None) -> str:
        account_id = quote_path_arg(account_id or self.default_account)
        account = self.get(f"accounts/{account_id}")
        if not account:
            return
        return account.json()

    def get_account_by_sis_id(self, sis_id: int) -> str:
        sis_lookup = quote_path_arg(f"sis_account_id:{sis_id}")
        account = self.get(f"accounts/{sis_lookup}")
        if not account:
            return
        return account.json()

    def list_sub_accounts(self,
                          account_id: Optional[str] = None,
                          recursive: Optional[bool] = False,
                          per_page: int =100):
        account_id = quote_path_arg(account_id or self.default_account)
        params = {
            "per_page": per_page,
            "recursive": recursive,
        }
        first_page = self.get(f"accounts/{account_id}/sub_accounts", params=params)
        for sub_account in self.depaginate(first_page):
            yield sub_account

    def list_courses(self,
                     account_id: Optional[str] = None,
                     per_page: int = 100,
                     **kwargs):
        account_id = quote_path_arg(account_id or self.default_account)
        params = {
            "per_page": per_page,
        }
        params.update(kwargs)
        first_page = self.get(f"accounts/{account_id}/courses", params=params)
        for course in self.depaginate(first_page):
            yield course

    def list_courses_by_user(
        self, user_id: Optional[str] = None, per_page: int = 100, **kwargs
    ):
        """list of active courses for this user,
        to view course list for others than your self,
        user should be is an observer or admin"""

        user_id = quote_path_arg(user_id or self.default_account)
        params = {
            "per_page": per_page,
        }
        params.update(kwargs)

        first_page = self.get(f"users/{user_id}/courses", params=params)
        for course in self.depaginate(first_page):
            yield course

    def create_account(self, data: dict, account_id: Optional[str] = None) -> str:
        """
        Create an account.

        https://canvas.instructure.com/doc/api/accounts.html#method.sub_accounts.create
        """
        account_id = quote_path_arg(account_id or self.default_account)
        return self.post(
            f"accounts/{account_id}/sub_accounts", json={"account": data}
        ).json()

    def update_account(self, account_id: str, data: dict) -> str:
        """
        Update an account.

        https://canvas.instructure.com/doc/api/accounts.html#method.accounts.update
        """
        account_id = quote_path_arg(account_id or self.default_account)
        return self.put(f"accounts/{account_id}", json={"account": data}).json()

    def delete_sub_account(self, sub_account_id: str, account_id: Optional[str] = None):
        account_id = quote_path_arg(account_id or self.default_account)
        sub_account_id = quote_path_arg(sub_account_id)
        self.delete(f"accounts/{account_id}/sub_accounts/{sub_account_id}").json()

    def create_term(self, data) -> str:
        account_id = quote_path_arg(ROOT_ACCOUNT)
        return self.post(
            f"accounts/{account_id}/terms", json={"enrollment_term": data}
        ).json()

    def get_course(self, course_id: int, account_id="self", **kwargs) -> str:
        account_id = quote_path_arg(account_id or self.default_account)
        course_id = quote_path_arg(course_id)
        params = {}
        params.update(kwargs)
        course = self.get(f"accounts/{account_id}/courses/{course_id}", params=params)
        if course is None:
            return
        return course.json()

    def get_course_by_sis_id(self, sis_course_id: int, **kwargs) -> str:
        sis_lookup = quote_path_arg(f"sis_course_id:{sis_course_id}")
        params = {}
        params.update(kwargs)
        course = self.get(f"courses/{sis_lookup}", params=params)
        if course is None:
            return
        return course.json()

    def create_course(self, data: dict, account_id: str) -> str:
        account_id = quote_path_arg(account_id or self.default_account)
        return self.post(f"accounts/{account_id}/courses", json={"course": data}).json()

    def update_course(self, data: dict, course_id: int) -> str:
        course_id = quote_path_arg(course_id)
        return self.put(f"courses/{course_id}", json={"course": data}).json()

    def get_course_section(self, section_id: int) -> str:
        section_id = quote_path_arg(section_id)
        section = self.get(f"sections/{section_id}")
        if section is None:
            return
        return section.json()

    def get_course_section_by_sis_id(self, section_id: int) -> str:
        sis_lookup = quote_path_arg(f"sis_section_id:{section_id}")
        section = self.get(f"sections/{sis_lookup}")
        if section is None:
            return
        return section.json()

    def create_course_section(self, data: dict, course_id: int) -> str:
        course_id = quote_path_arg(course_id)
        return self.post(
            f"courses/{course_id}/sections", json={"course_section": data}
        ).json()

    def update_course_section(self, data, section_id: int) -> str:
        section_id = quote_path_arg(section_id)
        return self.put(f"sections/{section_id}", json={"course_section": data}).json()

    def list_course_sections(self, course_id: int) -> list:
        course_id = quote_path_arg(course_id)
        return list(self.depaginate(self.get(f"courses/{course_id}/sections")))

    def list_enrollments_by_course(
        self,
        course_id: int,
        enrollment_type: Optional[str] = None,
        role: Optional[str] = None,
        state: Optional[str] = None,
        sis_account_id: Optional[str] = None,
        sis_course_id: Optional[str] = None,
        sis_section_id: Optional[str] = None,
        sis_user_id: Optional[str] = None,
        user_id: Optional[str] = None,
        per_page: int = 100,
    )  -> list:
        """List enrollments in a course.

        :param str/list enrollment_type:
            Filter by a list of enrollment types. Defaults to all.
        :param str/list role:
            Filter by a list of enrollment roles. Defaults to all.
        :param str/list state:
            Filter by enrollment states. Defaults to 'active' and 'invited'.
            Allowed values:
                active, invited, creation_pending, deleted, rejected,
                completed, inactive
            Allowed if user_id is supplied:
                current_and_invited, current_and_future, current_and_concluded
        :param str/list sis_account_id:
            Filter by SIS account ID(s). Does not look in sub accounts.
        :param str/list sis_course_id: Filter by SIS course ID(s).
        :param str/list sis_section_id: Filter by SIS section ID(s).
        :param str/list sis_user_id: Filter by SIS user ID(s).
        :param str user_id: Filter by user id.
        """
        course_id = quote_path_arg(course_id)
        params = {}
        if per_page:
            params["per_page"] = per_page
        if enrollment_type:
            params["type"] = enrollment_type
        if role:
            params["role"] = role
        if state:
            params["state"] = state
        if sis_account_id:
            params["sis_account_id"] = sis_account_id
        if sis_course_id:
            params["sis_course_id"] = sis_course_id
        if sis_section_id:
            params["sis_section_id"] = sis_section_id
        if sis_user_id:
            params["sis_user_id"] = sis_user_id
        if user_id:
            params["user_id"] = user_id
        return list(
            self.depaginate(self.get(f"courses/{course_id}/enrollments", params=params))
        )

    def list_enrollments_by_section(
        self,
        section_id: int,
        enrollment_type: Optional[str] = None,
        role: Optional[str] = None,
        state: Optional[str] = None,
        sis_account_id: Optional[str] = None,
        sis_course_id: Optional[str] = None,
        sis_section_id: Optional[str] = None,
        sis_user_id: Optional[str] = None,
        user_id: Optional[str] = None,
        per_page: int = 100,
    ) -> list:
        """List enrollments in a section.

        :param str/list enrollment_type:
            Filter by a list of enrollment types. Defaults to all.
        :param str/list role:
            Filter by a list of enrollment roles. Defaults to all.
        :param str/list state:
            Filter by enrollment states. Defaults to 'active' and 'invited'.
            Allowed values:
                active, invited, creation_pending, deleted, rejected,
                completed, inactive
        :param str/list sis_account_id:
            Filter by SIS account ID(s). Does not look in sub accounts.
        :param str/list sis_course_id: Filter by SIS course ID(s).
        :param str/list sis_section_id: Filter by SIS section ID(s).
        :param str/list sis_user_id: Filter by SIS user ID(s).
        :param str user_id: Filter by user id.
        """
        section_id = quote_path_arg(section_id)
        params = {}
        if per_page:
            params["per_page"] = per_page
        if enrollment_type:
            params["type"] = enrollment_type
        if role:
            params["role"] = role
        if state:
            params["state"] = state
        if sis_account_id:
            params["sis_account_id"] = sis_account_id
        if sis_course_id:
            params["sis_course_id"] = sis_course_id
        if sis_section_id:
            params["sis_section_id"] = sis_section_id
        if sis_user_id:
            params["sis_user_id"] = sis_user_id
        if user_id:
            params["user_id"] = user_id
        return list(
            self.depaginate(self.get(f"sections/{section_id}/enrollments", params=params))
        )

    def list_enrollments_by_user(
        self,
        user_id: int,
        enrollment_type: Optional[str] = None,
        role: Optional[str] = None,
        state: Optional[str] = None,
        sis_account_id: Optional[str] = None,
        sis_course_id: Optional[str] = None,
        sis_section_id: Optional[str] = None,
        sis_user_id: Optional[str] = None,
        enrollment_term_id: Optional[str] = None,
        per_page: int = 100,
    ) -> list:
        """List a user's enrollments.

        :param str/list enrollment_type:
            Filter by a list of enrollment types. Defaults to all.
        :param str/list role:
            Filter by a list of enrollment roles. Defaults to all.
        :param str/list state:
            Filter by enrollment states. Defaults to 'active' and 'invited'.
            Allowed values:
                active, invited, creation_pending, deleted, rejected,
                completed, inactive, current_and_invited, current_and_future,
                current_and_concluded
        :param str/list sis_account_id:
            Filter by SIS account ID(s). Does not look in sub accounts.
        :param str/list sis_course_id: Filter by SIS course ID(s).
        :param str/list sis_section_id: Filter by SIS section ID(s).
        :param str/list sis_user_id: Filter by SIS user ID(s).
        :param int enrollment_term_id: Filter by the specified enrollment term.
        """
        user_id = quote_path_arg(user_id)
        params = {}
        if per_page:
            params["per_page"] = per_page
        if enrollment_type:
            params["type"] = enrollment_type
        if role:
            params["role"] = role
        if state:
            params["state"] = state
        if sis_account_id:
            params["sis_account_id"] = sis_account_id
        if sis_course_id:
            params["sis_course_id"] = sis_course_id
        if sis_section_id:
            params["sis_section_id"] = sis_section_id
        if sis_user_id:
            params["sis_user_id"] = sis_user_id
        if enrollment_term_id:
            params["enrollment_term_id"] = enrollment_term_id
        return list(
            self.depaginate(self.get(f"users/{user_id}/enrollments", params=params))
        )

    def get_enrollment(self, enrollment_id, account_id=None) -> str:
        """Fetch enrollment by ID."""
        account_id = quote_path_arg(account_id or self.default_account)
        enrollment_id = quote_path_arg(enrollment_id)
        return self.get(f"accounts/{account_id}/enrollments/{enrollment_id}").json()

    def add_course_enrollment(
        self,
        user_id: int,
        course_id: int,
        enrollment_type: str = "StudentEnrollment",
        role_id: Optional[str] = None,
        enrollment_state: str ="active",
        course_section_id: Optional[int] = None,
    ) -> requests.Response:
        """
        Create a new user enrollment for a course.

        :param str user_id: ID of the user to be enrolled
        :param str enrollment_type:
            Enroll the user as a student, teacter, TA, observer, or designer.
            Allowed values:
                StudentEnrollment, TeacherEnrollment, TaEnrollment,
                ObserverEnrollment, DesignerEnrollment
        :param int role_id: Assign a custom course-level role.
        :param int course_section_id:
            ID of the course section to enroll the student in.
        """
        course_id = quote_path_arg(course_id)
        enrollment = {
            "user_id": user_id,
            "type": enrollment_type,
            "role_id": role_id,
            "enrollment_state": enrollment_state,
            "course_section_id": course_section_id,
        }
        return self.post(
            f"courses/{course_id}/enrollments", json={"enrollment": enrollment}
        )

    def add_section_enrollment(
        self,
        user_id: int,
        section_id: int,
        enrollment_type: str ="StudentEnrollment",
        role_id: int = None,
        enrollment_state: str ="active",
        limit_privileges_to_course_section: bool = False,
        start_at: Optional[str] = None,
        end_at: Optional[str] = None,
    ) -> requests.Response:
        """
        Create a new user enrollment for a section.

        :param str user_id: ID of the user to be enrolled
        :param str enrollment_type:
            Enroll the user as a student, teacter, TA, observer, or designer.
            Allowed values:
                StudentEnrollment, TeacherEnrollment, TaEnrollment,
                ObserverEnrollment, DesignerEnrollment
        :param int role_id: Assign a custom course-level role.
        :param datetime.datetime start_at: start time of the role
        :param datetime.datetime end_at: end time of the role
        """
        section_id = quote_path_arg(section_id)
        enrollment = {
            "user_id": user_id,
            "type": enrollment_type,
            "role_id": role_id,
            "enrollment_state": enrollment_state,
            "limit_privileges_to_course_section": limit_privileges_to_course_section,
        }
        if start_at:
            enrollment["start_at"] = start_at.isoformat()
        if end_at:
            enrollment["end_at"] = end_at.isoformat()
        return self.post(
            f"sections/{section_id}/enrollments", json={"enrollment": enrollment}
        )

    def delete_enrollment(self, course_id: int, enrollment_id: int, task: str ="deactivate") -> requests.Response:
        """
        Conclude, deactivate or delete an enrollment for a course.

        :param int course_id: Course ID for the enrollment
        :param int enrollment_id: Enrollment ID
        :param str task:
            Action to take on the enrollment. Default is to deactivate.
            Allowed values: conclude, delete, deactivate
        """
        course_id = quote_path_arg(course_id)
        enrollment_id = quote_path_arg(enrollment_id)
        return self.delete(
            f"courses/{course_id}/enrollments/{enrollment_id}", json={"task": task}
        )

    def reactivate_enrollment(self, course_id: int, enrollment_id: int) -> requests.Response:
        """
        Activates an inactive enrollment.

        :param int course_id: Course ID for the enrollment
        :param int enrollment_id: Enrollment ID
        """
        course_id = quote_path_arg(course_id)
        enrollment_id = quote_path_arg(enrollment_id)
        return self.put(f"courses/{course_id}/enrollments/{enrollment_id}/reactivate")

    def list_roles(self, account_id: Optional[str] = None):
        account_id = quote_path_arg(ROOT_ACCOUNT)
        params = {"per_page": 100}
        r = self.get(f"accounts/{account_id}/roles", params=params)
        for item in self.depaginate(r):
            yield item

    def get_role(self, role_id: int, account_id: Optional[str] = None) -> str:
        account_id = quote_path_arg(account_id or self.default_account)
        role_id = quote_path_arg(role_id)
        return self.get(f"accounts/{account_id}/roles/{role_id}").json()

    def create_role(self, role_name: str, account_id: Optional[str] = None) -> str:
        account_id = quote_path_arg(account_id or self.default_account)
        return self.post(
            f"accounts/{account_id}/roles", json={"label": role_name}
        ).json()
        # TODO: Should we activate the role here=??

    def activate_role(self, role_id: int, account_id: Optional[str] = None) -> str:
        account_id = quote_path_arg(account_id or self.default_account)
        role_id = quote_path_arg(role_id)
        return self.post(f"accounts/{account_id}/roles/{role_id}/activate").json()

    def list_course_modules(self, course_id: int, student_id=None, per_page: int = 100) -> list:
        """List course modules.

        :param int student_id:
            Include module completion information for the student with this ID.
        """
        course_id = quote_path_arg(course_id)
        params = {}
        if per_page:
            params["per_page"] = per_page
        if student_id:
            params["student_id"] = student_id
        return list(
            self.depaginate(self.get(f"courses/{course_id}/modules", params=params))
        )

    def list_calendar_events(self, params: Optional[dict] = None):
        """
        List calendar events for current user

        https://canvas.instructure.com/doc/api/calendar_events.html#method.calendar_events_api.index

        Parameter       Type    Description
        type            string  Defaults to “event”. Allowed values: event, assignment
        start_date      Date    Only return events since the start_date (inclusive). Defaults to today. The value should be formatted as: yyyy-mm-dd or ISO 8601 YYYY-MM-DDTHH:MM:SSZ.
        end_date        Date    Only return events before the end_date (inclusive). Defaults to start_date. The value should be formatted as: yyyy-mm-dd or ISO 8601 YYYY-MM-DDTHH:MM:SSZ. If end_date is the same as start_date, then only events on that day are returned.
        undated         boolean Defaults to false (dated events only). If true, only return undated events and ignore start_date and end_date.
        all_events      boolean Defaults to false (uses start_date, end_date, and undated criteria). If true, all events are returned, ignoring start_date, end_date, and undated criteria.
        context_codes[] string  List of context codes of courses/groups/users whose events you want to see. If not specified, defaults to the current user (i.e personal calendar, no course/group events). Limited to 10 context codes, additional ones are ignored. The format of this field is the context type, followed by an underscore, followed by the context id. For example: course_42
        excludes[]      Array   Array of attributes to exclude. Possible values are “description”, “child_events” and “assignment”
        important_dates boolean Defaults to false. If true, only events with important dates set to true will be returned.
        """
        qparams = {"per_page": 100}
        if params:
            qparams.update(params)
        first_page = self.get(f"calendar_events", params=qparams)
        for event in self.depaginate(first_page):
            yield event

    def list_user_calendar_events(
        self, user_id: Optional[str] = None, params: Optional[dict] = None
    ):
        """
        List calendar events for a user.

        If no user_id is given, the configured default user is used.

        https://canvas.instructure.com/doc/api/calendar_events.html#method.calendar_events_api.user_index

        Parameter                  Type    Description
        type                       string  Defaults to “event”. Allowed values: event, assignment
        start_date                 Date    Only return events since the start_date (inclusive). Defaults to today. The value should be formatted as: yyyy-mm-dd or ISO 8601 YYYY-MM-DDTHH:MM:SSZ.
        end_date                   Date    Only return events before the end_date (inclusive). Defaults to start_date. The value should be formatted as: yyyy-mm-dd or ISO 8601 YYYY-MM-DDTHH:MM:SSZ. If end_date is the same as start_date, then only events on that day are returned.
        undated                    boolean Defaults to false (dated events only). If true, only return undated events and ignore start_date and end_date.
        all_events                 boolean Defaults to false (uses start_date, end_date, and undated criteria). If true, all events are returned, ignoring start_date, end_date, and undated criteria.
        context_codes[]            string  List of context codes of courses/groups/users whose events you want to see. If not specified, defaults to the current user (i.e personal calendar, no course/group events). Limited to 10 context codes, additional ones are ignored. The format of this field is the context type, followed by an underscore, followed by the context id. For example: course_42
        excludes[]                 Array   Array of attributes to exclude. Possible values are “description”, “child_events” and “assignment”
        submission_types[]         Array   When type is “assignment”, specifies the allowable submission types for returned assignments. Ignored if type is not “assignment” or if exclude_submission_types is provided.
        exclude_submission_types[] Array   When type is “assignment”, specifies the submission types to be excluded from the returned assignments. Ignored if type is not “assignment”.
        important_dates            boolean Defaults to false If true, only events with important dates set to true will be returned.
        """
        user_id = quote_path_arg(user_id or self.default_account)
        qparams = {"per_page": 100}
        if params:
            qparams.update(params)
        first_page = self.get(f"users/{user_id}/calendar_events", params=qparams)
        for event in self.depaginate(first_page):
            yield event

    def create_calendar_event(self, data: dict) -> str:
        """
        Create a calendar event.

        https://canvas.instructure.com/doc/api/calendar_events.html#method.calendar_events_api.create

        Parameter                                         Type     Description
        calendar_event[context_code] Required             string   Context code of the course/group/user whose calendar this event should be added to.
        calendar_event[title]                             string   Short title for the calendar event.
        calendar_event[description]                       string   Longer HTML description of the event.
        calendar_event[start_at]                          DateTime Start date/time of the event.
        calendar_event[end_at]                            DateTime End date/time of the event.
        calendar_event[location_name]                     string   Location name of the event.
        calendar_event[location_address]                  string   Location address
        calendar_event[time_zone_edited]                  string   Time zone of the user editing the event. Allowed time zones are IANA time zones or friendlier Ruby on Rails time zones.
        calendar_event[all_day]                           boolean  When true event is considered to span the whole day and times are ignored.
        calendar_event[child_event_data][X][start_at]     DateTime Section-level start time(s) if this is a course event. X can be any identifier, provided that it is consistent across the start_at, end_at and context_code
        calendar_event[child_event_data][X][end_at]       DateTime Section-level end time(s) if this is a course event.
        calendar_event[child_event_data][X][context_code] string   Context code(s) corresponding to the section-level start and end time(s).
        calendar_event[duplicate][count]                  number   Number of times to copy/duplicate the event. Count cannot exceed 200.
        calendar_event[duplicate][interval]               number   Defaults to 1 if duplicate `count` is set. The interval between the duplicated events.
        calendar_event[duplicate][frequency]              string   Defaults to “weekly”. The frequency at which to duplicate the event. Allowed values: daily, weekly, monthly
        calendar_event[duplicate][append_iterator]        boolean  Defaults to false. If set to `true`, an increasing counter number will be appended to the event title when the event is duplicated. (e.g. Event 1, Event 2, Event 3, etc)
        """
        data = {"calendar_event": data}
        return self.post("calendar_events", json=data).json()

    def get_calender_event(self, event_id: int) -> Optional[str]:
        """
        Get a single calendar event or assignment.

        https://canvas.instructure.com/doc/api/calendar_events.html#method.calendar_events_api.show
        """
        event_id = quote_path_arg(event_id)
        event = self.get(f"calendar_events/{event_id}")
        if event:
            return event.json()
        return None

    def reserve_time_slot(
        self,
        event_id: int,
        participant_id: Optional[str] = None,
        comments: Optional[str] = None,
        cancel_existing: Optional[bool] = None,
    ) -> str:
        """
        Reserve a time slot.

        https://canvas.instructure.com/doc/api/calendar_events.html#method.calendar_events_api.reserve

        Parameter       Type    Description
        participant_id  string  User or group id for whom you are making the reservation (depends on the participant type). Defaults to the current user (or user's candidate group).
        comments        string  Comments to associate with this reservation
        cancel_existing boolean Defaults to false. If true, cancel any previous reservation(s) for this participant and appointment group.
        """
        event_id = quote_path_arg(event_id)
        if participant_id:
            participant_id = quote_path_arg(participant_id)
            url = f"calendar_events/{event_id}/reservations/{participant_id}"
        else:
            url = f"calendar_events/{event_id}/reservations"
        data = {}
        if comments:
            data["comments"] = comments
        if cancel_existing:
            data["cancel_existing"] = cancel_existing
        return self.post(url, json=data).json()

    def update_calendar_event(self, event_id: int, data: dict) -> str:
        """
        Update a calendar event.

        https://canvas.instructure.com/doc/api/calendar_events.html#method.calendar_events_api.update

        Parameter                                         Type     Description
        calendar_event[context_code]                      string   Context code of the course/group/user to move this event to. Scheduler appointments and events with section-specific times cannot be moved between calendars.
        calendar_event[title]                             string   Short title for the calendar event.
        calendar_event[description]                       string   Longer HTML description of the event.
        calendar_event[start_at]                          DateTime Start date/time of the event.
        calendar_event[end_at]                            DateTime End date/time of the event.
        calendar_event[location_name]                     string   Location name of the event.
        calendar_event[location_address]                  string   Location address
        calendar_event[time_zone_edited]                  string   Time zone of the user editing the event. Allowed time zones are IANA time zones or friendlier Ruby on Rails time zones.
        calendar_event[all_day]                           boolean  When true event is considered to span the whole day and times are ignored.
        calendar_event[child_event_data][X][start_at]     DateTime Section-level start time(s) if this is a course event. X can be any identifier, provided that it is consistent across the start_at, end_at and context_code
        calendar_event[child_event_data][X][end_at]       DateTime Section-level end time(s) if this is a course event.
        calendar_event[child_event_data][X][context_code] string   Context code(s) corresponding to the section-level start and end time(s).
        """
        data = {"calendar_event": data}
        return self.put(f"calendar_events/{event_id}", json=data).json()

    def delete_calendar_event(self, event_id: int, cancel_reason: Optional[str] = None) -> str:
        """
        Delete a calendar event.

        https://canvas.instructure.com/doc/api/calendar_events.html#method.calendar_events_api.destroy

        Parameter     Type   Description
        cancel_reason string Reason for deleting/canceling the event.
        """
        params = {}
        if cancel_reason:
            params["cancel_reason"] = cancel_reason
        event_id = quote_path_arg(event_id)
        return self.delete(f"calendar_events/{event_id}", params=params).json()

    def create_course_timetable(self, course_id: int, data: dict) -> str:
        """
        Set a course timetable.

        https://canvas.instructure.com/doc/api/calendar_events.html#method.calendar_events_api.set_course_timetable

        Parameter                                      Type   Description
        timetables[course_section_id][]                Array  An array of timetable objects for the course section specified by course_section_id. If course_section_id is set to “all”, events will be created for the entire course.
        timetables[course_section_id][][weekdays]      string A comma-separated list of abbreviated weekdays (Mon-Monday, Tue-Tuesday, Wed-Wednesday, Thu-Thursday, Fri-Friday, Sat-Saturday, Sun-Sunday)
        timetables[course_section_id][][start_time]    string Time to start each event at (e.g. “9:00 am”)
        timetables[course_section_id][][end_time]      string Time to end each event at (e.g. “9:00 am”)
        timetables[course_section_id][][location_name] string A location name to set for each event
        """
        course_id = quote_path_arg(course_id)
        return self.post(
            f"courses/{course_id}/calendar_events/timetable", json=data
        ).json()

    def get_course_timetable(self, course_id: int) -> str:
        """
        Get course timetable.
        https://canvas.instructure.com/doc/api/calendar_events.html#method.calendar_events_api.get_course_timetable
        """
        course_id = quote_path_arg(course_id)
        return self.get(f"courses/{course_id}/calendar_events/timetable").json()

    def create_course_timetable_events(self, course_id: int, data: dict) -> str:
        """
        Create or update events directly for a course timetable.
        https://canvas.instructure.com/doc/api/calendar_events.html#method.calendar_events_api.set_course_timetable_events

        Parameter               Type     Description
        course_section_id       string   Events will be created for the course section specified by course_section_id. If not present, events will be created for the entire course.
        events[]                Array    An array of event objects to use.
        events[][start_at]      DateTime Start time for the event
        events[][end_at]        DateTime End time for the event
        events[][location_name] string   Location name for the event
        events[][code]          string   A unique identifier that can be used to update the event at a later time If one is not specified, an identifier will be generated based on the start and end times
        events[][title]         string   Title for the meeting. If not present, will default to the associated course's name
        """
        course_id = quote_path_arg(course_id)
        return self.post(
            f"courses/{course_id}/calendar_events/timetable_events", json=data
        ).json()


def get_client(config_dict: dict) -> CanvasClient:
    """
    Get a Canvas client from configuration.
    """
    alts = {
        "url": ["baseurl", "base_url"],
        "token": ["auth_token"],
        "account": ["root_account_id"],
        "headers": ["extra_headers"],
    }

    for config_key, alt_keys in alts.items():
        if config_key in config_dict:
            continue
        for key in alt_keys:
            if key in config_dict:
                warnings.warn(
                    f"cerebrum client config '{key}' is deprecated, "
                    f"please use '{config_key}'",
                    category=DeprecationWarning,
                )
                config_dict[config_key] = config_dict[key]
                break

    return CanvasClient(
        config_dict["url"],
        token=config_dict.get("token"),
        default_account=config_dict.get("account"),
        headers=config_dict.get("headers"),
        rewrite_url=config_dict.get("rewrite_url"),
    )
