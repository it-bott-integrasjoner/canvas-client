from .client import CanvasClient
from .version import get_distribution

__all__ = ["Canvas", "CanvasClient"]
__version__ = get_distribution().version


# Compatibility
Canvas = CanvasClient
